package Specs;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import Helper.Constants;

public class TestRunner {
		
	WebDriver driver;
		
		@Test
		public void openBrowser()
		{
			System.setProperty("webdriver.chrome.driver",Constants.driverChromePath );
//			//System.setProperty(TestConstants.driverChrome,TestConstants.driverChromePath );
			driver = new ChromeDriver ();
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.HOURS);
			driver.navigate().to(Constants.WebUrl);
			driver.manage().window().maximize();
		}

	}