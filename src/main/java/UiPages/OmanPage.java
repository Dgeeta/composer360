package UiPages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;
import org.apache.log4j.Logger;

import Helper.BaseClass;
import Helper.Constants;
import Helper.Log4j;

public class OmanPage extends BaseClass{
	
	By numberOfSkus= By.xpath(Constants.numberOfSkus);
	By zoomFeature= By.xpath(Constants.zoomFeature);
	By LargerImageToggle=By.xpath("//*[@id ='owFullscreenToggle']");
	By cancelButton=By.xpath("//*[@class='owFullscreenCloseButton pointer']");
	By viewTop=By.xpath("//*[@id='owTopDownToggle']");
	By DimensionToggle=By.xpath("//*[@id='owDimensionsToggle']");
	By hiddenTopToggle=By.xpath("//*[@id='topviewstatus']");
	By largerImageCanvasId=By.xpath("//*[@id='owViewport']");
	By hiddenRotationButton=By.xpath("//button[@id ='rotationstatus']");
	By hiddenZoomToggle=By.xpath("//*[@id='zoomstatus']");
	By hiddenDimensionToggle=By.xpath("//*[@id='dimensionsstatus']");
			
	
	public OmanPage(WebDriver driver) {
	    super(driver);
	}
	
	public void getTopViewAttributeValue() {
		WebElement hiddenEle= driver.findElement(hiddenTopToggle);
		String getvalue=hiddenEle.getAttribute("value");
		log.info("Fetching the Current status of the Rendered Image :"+getvalue);
		if (getvalue.equals("off")){
			log.info("currently, The Image is in -View front Mode ");
			}
		else
		    log.info("currently, The Image is in -View Top Mode ");
		}
	
	public void getTopViewAttributedataFrame() {
		WebElement hiddenEle= driver.findElement(hiddenTopToggle);
		String getDataFrame=hiddenEle.getAttribute("data-topviewframe");
		log.info("Fetching the Current Data frame of the Rendered Image :"+getDataFrame);
	}
	
	public void clickOnTopToggle() throws InterruptedException {
		log.info("Going to click on View Top Toggle");
		clickElement(viewTop);
		Thread.sleep(3000);
		}
	
	public void clickOnViewFrontToggle2() throws InterruptedException {
		log.info("Going to click on View Front Toggle");
		clickElement(viewTop);
		Thread.sleep(2000);
		}
	
	
	
	public void clickOnviewTopImage() {
		WebElement hiddenEle= driver.findElement(hiddenTopToggle);
		//String datFrame=getTopViewAttributedataFrame(hiddenEle);
		getCurrentViewMode(hiddenEle);
		log.info("Going to click on - View Top Image");
		WebElement viewTopToggle= driver.findElement(viewTop);
		clickElement(viewTop);
		//visibilityOfElement(viewTop);
		getNewViewMode(hiddenEle);
		log.info("The View Front Button is Highlighted and Its current Attribute Status is: "+viewTopToggle.getAttribute("class"));
		
	}
	
	public void clickOnViewFrontToggle() {
		clickElement(viewTop);
		WebElement viewFrontToggle= driver.findElement(viewTop);
		log.info("The Image is in Normal, View -Front view Mode ");
		log.info("The View Front Button is Not Highlighted and Its current Attribute Status is: "+viewFrontToggle.getAttribute("class"));
		
		
	}
	
	
	
	
	
	
	
	public void viewTopImage() throws InterruptedException{
		WebElement ele= driver.findElement(hiddenTopToggle);
		getCurrentViewMode(ele);
		log.info("Going to click on - View Top Image");
		clickHiddenElement(ele);
		getNewViewMode(ele);
		log.info("Now Going to click on - View large Image button, "
				+ "when Image is already opened in Top View");
		viewLargerImageFunc();
		log.info("Going to click on - View Front Image");
		//WebElement eles=driver.findElement(viewTop);
		clickElement(viewTop);
		log.info("The Image is in Normal, View -Front view Mode ");
		getCurrentViewMode(ele);
		
		}
	
	
	public void getCurrentViewMode(WebElement ele) {
		String currentViewTopMode= ele.getAttribute("value");
		log.info("Getting the current Element Attribute, The Value is : "+currentViewTopMode);
		if (currentViewTopMode.equals("off")){
				log.info("currently, The Image is in -View front Mode ");	
		}
		
		else
			log.error("currently, Can't determine the Mode of the Image ");
		}
	
	public void getNewViewMode(WebElement ele) {
		String newViewTopMode= ele.getAttribute("value");
		log.info("Getting the Element Attribute "+newViewTopMode);
		if (newViewTopMode.equals("on")){
				log.info("currently, The Image is in -View Top Mode ");	
		}
		
		else
			log.error("currently, Can't determine the Mode of the Image ");
		}
	
	public void clickHiddenElement(WebElement ele) {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", ele);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	

	





	public void validationOfZoomToggle() throws InterruptedException {
		WebElement ele = driver.findElement(zoomFeature);
		WebElement hiddenEle= driver.findElement(hiddenZoomToggle);
		String originalImageResolution=getImageResolution(hiddenEle);
		log.info("Fetching the Resolution of Image, The current resolution is: "+originalImageResolution);
		log.info("Going to check if the button is Highlighted or Not");
		getZoomToggleHighlightStatus(ele);
		log.info("Now, Going to click on Zoom In Toggle");
		clickElement(zoomFeature);
		Thread.sleep(1000);
		log.info(" After click on Zoom in Toggle, going to check if the button is Highlighted or Not");
		getZoomToggleHighlightStatus(ele);
		String zoomImageResolution=getImageResolution(hiddenEle);
		log.info("Now the current resolution changed to: "+zoomImageResolution);
		compareImageResolution(zoomImageResolution, originalImageResolution);
		}
	
	public void validationOfZoomOutToggle() throws InterruptedException {
		WebElement ele = driver.findElement(zoomFeature);
		WebElement hiddenEle= driver.findElement(hiddenZoomToggle);
		String originalImageResolution=getImageResolution(hiddenEle);
		log.info("Fetching the Resolution of Image, The current resolution is: "+originalImageResolution);
		log.info("Going to check if the button is Highlighted or Not");
		getZoomToggleHighlightStatus(ele);
		log.info("Now, Going to click on Zoom Out Toggle");
		clickElement(zoomFeature);
		log.info(" After click on Zoom out Toggle, going to check if the button is Highlighted or Not");
		getZoomToggleHighlightStatus(ele);
		String zoomImageResolution=getImageResolution(hiddenEle);
		log.info("Now the current resolution changed to: "+zoomImageResolution);
		compareImageResolution(zoomImageResolution, originalImageResolution);
		}
	
		
	public void getZoomToggleHighlightStatus(WebElement ele) {
		String buttonHighlightStatus=ele.getAttribute("class");
		if(buttonHighlightStatus.equals("active")) {
			log.info("The Zoom button is Highlighted");
			}
		else
			log.info("The Zoom button is not yet Highlighted");
		}
	
	public void compareImageResolution(String zoomedImage, String originalImage) {
		if (zoomedImage.equals(originalImage)) {
			log.info("Both the Resolutions are similar");	
		}
		else 
			log.info("The Image Resolution got changed");
		
	}
			
	public String getImageResolution(WebElement hiddenEle) {
		String zoomHighlightedStatus=hiddenEle.getAttribute("data-resolution");
		return (zoomHighlightedStatus);
		
	}
		

		public void skuDimensionImage() throws InterruptedException {
		clickElement(DimensionToggle);
		Thread.sleep(2000);

		}

		public void verifyImagePresent(String sku) {
		List<WebElement> total_images = driver.findElements(By.tagName("img"));
		}


		public void clickOnViewFront() throws InterruptedException {
	//	clickElement (HiddenTopToggle) ;
		Thread.sleep(2000);
		}

		 public List<WebElement> countNumberOfSkus(){
			 List<WebElement> element = driver.findElements(numberOfSkus);
			 int skuCount = element.size();
			 System.out.println("The No. of Skus are :"+skuCount);
			 return element;
			 }
		 
		 
		 public String[] getCurrentImageAttributes(WebElement ele) {
			 log.info("Getting information on Current Image attributes");
				String currentAttributes[] = new String[2];
				String originalImageWidth=ele.getAttribute("width");
				String originalImageHeight=ele.getAttribute("height");
				String currentStyle=ele.getAttribute("style");
				log.info("The Image Dimensions are :"+originalImageWidth);
				log.info("The Image Dimensions are :"+originalImageHeight);
				log.info("Fetching current Image Resolution :"+currentStyle);
				currentAttributes[0]=originalImageWidth;
				currentAttributes[1]=originalImageHeight;
				return (currentAttributes);
				 }
		 
		 public String[] getNewImageAttributes(WebElement ele) {
			 log.info("The Image is in- large View Mode and here are the New Image attributes");
			   String newImageWidth=ele.getAttribute("width");
			   String newImageHeight=ele.getAttribute("height");
			   String newImageattributes[] = new String[2];
			   String newStyle=ele.getAttribute("style");
				log.info("The Image Dimensions are :"+newImageWidth);
				log.info("The Image Dimensions are :"+newImageHeight);
				log.info("Fetching New Image Resolution :"+newStyle);
				newImageattributes[0]=newImageWidth;
				newImageattributes[1]=newImageHeight;
				return (newImageattributes);
		 }

		 
		 
		 public void verifyImageAttributes(String[] currentImage, String[] newImage) {
			 if ((currentImage[0] !=newImage[0]) && (newImage[0] !=null) && 
						(currentImage[1] !=newImage[1]) && (newImage[1] !=null)) {
					log.info("The New Image Dimensions are :"+newImage[0]);
					log.info("The New Image Dimensions are :"+newImage[1]);
					log.info("Image Resolution changed Successfully");
					}
				else {
					log.error("The Image is either Blank or the resolution didn't change");
				} 
			 }
		 
		 public void validateCancelButtonVisibility() {
			 log.info("Going to validate if the -Cancel Button is available");
			 WebElement elementDisplay= driver.findElement(cancelButton);
			 Boolean value =elementDisplay.isDisplayed();
			 if(value.booleanValue()){
			 log.info("The Cancel button is available in the Page");
				}
			 else {
				 log.error("The Cancel button is not available in the Page");
				  }
		 }
		 
		public void clickCancelButton() {
			log.info("Going to click on -Cancel Button in View Large Image Mode");
			WebElement cancelButtn= driver.findElement(cancelButton);
			cancelButtn.click();
			log.info("Image is back in Normal Mode");
		}
		
		public String getRotationTypeOfImage() {
			log.info("Getting the Rotation Type of Image");
			WebElement ele= driver.findElement(hiddenRotationButton);
			String rotationType=ele.getAttribute("data-rotationtype");
			log.info("The Rotation Type of the OMAN is : "+rotationType);
			return(rotationType);
			}
		

		public String getFrameSequenceOfImage() {
			log.info("Getting the Rotation Type of Image");
			WebElement ele= driver.findElement(hiddenRotationButton);
			String dataFrameSeq=ele.getAttribute("data-framesequence");
			log.info("Here is the List of Frame Sequence : "+dataFrameSeq);
			return(dataFrameSeq);
			}
		
		public void validateImageRotation() {
			int numberOfRotation=20;
			WebElement ele= driver.findElement(hiddenRotationButton);
			String value=ele.getAttribute("value");
			SoftAssert softAssert = new SoftAssert();
			softAssert.assertEquals("1", value);
			log.info("Going to Rotate the Image ");
			if (getRotationTypeOfImage().equals("360"))
			{
				numberOfRotation = 31;
			}
			for (int i = 0; i < numberOfRotation; i++) {	
				clickHiddenElement(ele);
				log.info("The Image is rotated and its value is "+ele.getAttribute("value"));
				}
			}
		
		public void viewLargerImageFunc() throws InterruptedException{
			WebElement ele= driver.findElement(largerImageCanvasId);
			String[] currentImage=getCurrentImageAttributes (ele);
			log.info("Going to click on - View larger Image");
			clickElement(LargerImageToggle);
			Thread.sleep(1500);
			String[] newImage=getNewImageAttributes(ele);
			verifyImageAttributes(currentImage,newImage);
			validateCancelButtonVisibility();
			clickCancelButton();
			log.info("Test Execution for Click on View large Image Mode Executed Successfully");
			}
		
		public void getAttributesOfDimensionToggle() {
			WebElement ele= driver.findElement(hiddenDimensionToggle);
			log.info(
					"*****************************************************************************************************");
			getDimensionDataFrame(ele);
			log.info("Going to check if the Toggle is Highlighted or Not");
			getDimensionToggleHighlightStatus(ele);
		}
		
		public void getDimensionDataFrame(WebElement ele) {
			String dataDimensionFrame=ele.getAttribute("data-dimensionframe");
			log.info("Going to fetch current Data Dimension Frame Information, The Data Dimension Frame used is : "+dataDimensionFrame);
		}
		
		public void clickDimensionToggle() throws InterruptedException {
			WebElement ele= driver.findElement(DimensionToggle);
			log.info("Going to click on Dimension Toggle");
			clickElement(DimensionToggle);
			Thread.sleep(1900);
		}
		
		public void getDimensionToggleHighlightStatus(WebElement ele) {
			String buttonHighlightStatus=ele.getAttribute("value");
			if(buttonHighlightStatus.equals("on")) {
				log.info("The Dimension button is Highlighted");
				}
			else
				log.info("The Dimension button is not yet Highlighted");
			}
	

}
