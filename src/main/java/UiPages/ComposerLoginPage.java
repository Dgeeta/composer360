package UiPages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;

import com.aventstack.extentreports.ExtentReports;

import Helper.BaseClass;
import Helper.Constants;
import Helper.PropertyFileUtility;

public class ComposerLoginPage extends BaseClass{
	
	By outwardLoginBtns= By.xpath(Constants.outwardLoginBtn);
	By gmailEmailIdBtn= By.xpath(Constants.gmailEmailId);
	By nextBtn= By.xpath(Constants.nextBtn);
	By gmailPwdBtn= By.xpath(Constants.gmailpwd);
	By pwdNextBtn= By.xpath(Constants.pwdNextBtn);
	By groupImage= By.xpath(Constants.groupImage);
	By WsiPb360links=By.xpath(Constants.WsiPb360clickablelink);
	By filtertext=By.xpath(Constants.filtertext);
	By OutwardUsername = By.xpath(Constants.OutwardUserName);
	By OutwardPassword = By.xpath(Constants.OutwardPassword);
	By OutwardLoginButton = By.xpath(Constants.OutwardLoginButton);
	

	
	
	 public ComposerLoginPage(WebDriver driver) {
	        super(driver);
	    }
	

	public void loginIntoApplication(String username, String password) throws InterruptedException{
		log.info("Going to Enter the credentials, Where user name is "+username+" And Password is :"+password);
		driver.findElement(OutwardUsername).sendKeys(username);
		driver.findElement(OutwardPassword).sendKeys(password);
		clickElement(OutwardLoginButton);
		String PageTitle ="Admin Panel";
		//Assert.assertEquals(driver.getTitle(), PageTitle);
		if(PageTitle.equals(driver.getTitle())){
			log.info("Verifed the Page Title, The Page Title is "+driver.getTitle());}
			else {
				log.error("The Page Title isn't correct ");
				}
		}
	

	public void searchForOman(){
		WebElement ele= driver.findElement(filtertext);
		ele.sendKeys("abac");
		ele.sendKeys(Keys.ENTER);
	}
	
	public void clickComposerLink(String composerLink){
		driver.get(composerLink);
		//driver.navigate().refresh();
		
		
	}
	
}
