package Helper;

import org.openqa.selenium.WebDriver;

public class Constants  {
	
	
	
	public static final int TIMEOUT = 5;
    public static final int POLLING = 100;

	public static String WebUrl = "https://test-composer.outwardinc.com/";
	public static String driverChromePath= ".\\src\\main\\resources\\ChromeDriver\\Latest\\chromedriver.exe";
	public static String chromeDriver="webdriver.chrome.driver";
	public static String viewUrl="http://test-composer.outwardinc.com/view/WSI_WE_CT3";
	public static String TestDatapath = ".\\src\\main\\resources\\TestData";
	public static String configPath = ".\\src\\main\\resources\\Configfile\\Configuration.properties";
    public static String wsiPath="./src/main/resources/TestData/WSI PB 360.xlsx";
	
	/** --------------------Login Page-------------------- */
	
    public static String OutwardUserName="//input[@name ='username']";
    public static String OutwardPassword="//input[@name ='password']";
    public static String OutwardLoginButton="//*[@name ='login']";
    public static String outwardLoginBtn="//a[text()='Outward Login']";
	public static String gmailEmailId="//*[@class='whsOnd zHQkBf']";
	public static String gmailpwd="//*[@type='password'][@name ='password']";
	public static String nextBtn="//*[text()='Next']";
	public static String pwdNextBtn="//*[@id='passwordNext']";
	
	/** --------------------Admin Page-------------------- */
	public static String groupImage="//*[@class='fa fa-cubes fa-5x']";
	public static String WsiPb360clickablelink= "//*[text()='WSI PB360']";
	
	/** --------------------Product Page-------------------- */
	public static String filtertext="//*[@type='text'][@name='filter']";
	public static String searchBtn="//*[@class='btn btn-default'][@type='submit']";
	
	/** --------------------Composer Page-------------------- */
	public static String numberOfSkus="//*[@class='owSelectorCategoryInner']/div";
	public static String zoomFeature="//*[@id='owInlineZoomToggle']";
	
	
	
	
	

}
