package Helper;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseClass{
	private WebDriverWait wait;
	protected WebDriver driver;
	protected Logger log = Logger.getLogger("devpinoyLogger");
	
	
	
	public BaseClass(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Constants.TIMEOUT, Constants.POLLING);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.TIMEOUT), this);
    }
	
	
	void waitForElementToAppear(By locator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
	
	public void visibilityOfElement(By elementBy) {
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementBy));
	}
	
	/* Click on element */
	public void clickElement(By elementBy) {
		visibilityOfElement(elementBy);
		driver.findElement(elementBy).click();
	
}
	/* Read element */
	public String readText(By elementBy) {
		visibilityOfElement(elementBy);
		return driver.findElement(elementBy).getText();

	}
	
	public void verifyImagePresent(){
	}
	
	
	
	public String readpropertyfile(String configElement){
		
		String Element =PropertyFileUtility.getProperty(configElement);
		return Element;
	}

	
//	/* This is method is to take a screenshot of the page */
//	public void takeScreenshot(WebDriver driver, String screenshottaken) throws Exception {
//
//		try {
//
//			TakesScreenshot screen = (TakesScreenshot) TestConstant.driver;
//			File srcfile = screen.getScreenshotAs(OutputType.FILE);
//			FileUtils.copyFile(srcfile, new File("./screenshot/" + screenshottaken + ".jpeg"));
//			System.out.println("Screenshot" + "---" + screenshottaken + ".jpeg");
//
//		} catch (WebDriverException ex) {
//			ex.printStackTrace();
//			System.out.println("Exception occure while taking a screenshot" + ex.getMessage());
//		}
//
//	}
//	
	/* @AfterMethod */
//	public void failTestCase(ITestResult result) throws Exception {
//
//		if (ITestResult.FAILURE == result.getStatus()) {
//
//			generic.takeScreenshot(TestConstant.driver, result.getName());
//		}
//
//	}
	
	
	public void waitWebDriver() {

		WebDriverWait wait = new WebDriverWait(driver, 2000);

	}

	
	

}
