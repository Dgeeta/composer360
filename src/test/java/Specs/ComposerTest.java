package Specs;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.internal.PropertyUtils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.Status;

import Helper.BaseClass;
import Helper.Constants;
import Helper.Log4j;
import Helper.PropertyFileUtility;
import UiPages.ComposerLoginPage;
import UiPages.OmanPage;

public class ComposerTest extends BaseTest{
	
	ComposerLoginPage page;
	OmanPage omanPage ;
	
	public ComposerTest() {
		super();
	}
	
	@BeforeClass
	public void startTestCase() throws Exception {
		//DOMConfigurator.configure("C:\\Users\\geeta.dhami\\Desktop\\Desktop\\Composer360\\log4j.xml");
		DOMConfigurator.configure(PropertyFileUtility.getProperty("path_log_xml"));
		Log4j.info(
				"*****************************************************************************************************");
		Log4j.info("XXXXXXXXXXXXXXXXXXXXXXX                  " + "-S-T-A-R-T-"
				+ "                 XXXXXXXXXXXXXXXXXXXXXX");
		Log4j.info("XXXXXXXXXXXXXXXXXXXXXXX           " + "HOME PAGE" + "           XXXXXXXXXXXXXXXXXXXXXX");
		InitializeWebDriver(PropertyFileUtility.getProperty("browser"), PropertyFileUtility.getProperty("url"));
		page = new ComposerLoginPage(getDriver());
		logger =extent.createTest("Test Case -To test the Login in Application");
		logger.log(Status.INFO, "Going to Login into Composer Application");
		page.loginIntoApplication
		(PropertyFileUtility.getProperty("username"), PropertyFileUtility.getProperty("password"));
		logger.log(Status.PASS, "Successfully logged into Composer Application");
		}
	 	
	
	
	@Test(dataProvider="getDataFromExcel")
	public void Tc_2TestTheNoOfSkus(String OMAN, String ComposerLink){
		
		logger =extent.createTest("Test case- To verify No. of Skus available for the OMAN: "+OMAN );
		//page.clickComposerLink(PropertyFileUtility.getProperty("wsi360link")+OMAN);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.HOURS);
		String TestData;
		String viewUrl = Constants.viewUrl;
		String url =Constants.TestDatapath;
		omanPage = new OmanPage(getDriver());
		System.out.println("The Composer Link Url is "+ComposerLink);
		page.clickComposerLink(ComposerLink);
	}
	
		//List<WebElement> Sku=omaPage.countNumberOfSkus();
		//logger.log(Status.INFO, "Total No. of Skus available for the Oman: "+OMAN+" is :"+Sku.size());
		/*
		for (int i=0; i<=0;i++){
		logger.info("The SKU to be Verified is :" + Sku.get(i).getText());
		Sku.get(i).click();
		*/
		   // Tc_3ToTestViewLargeImage(OMAN,Sku.get(i).getText());
//		     Tc_3_1ToTestViewLargeImagewithTopView(OMAN, Sku.get(i).getText());
//		     Tc_4ToTestZoomInFeature(OMAN, Sku.get(i).getText());
//		     Tc_5ToTestZoomOutFeature(OMAN, Sku.get(i).getText());
//		     Tc_5_1ToTestZoomInFeaturewithViewTop(OMAN, Sku.get(i).getText());
//		    // Tc_6ToTestZoomInFeatureWithDimenion(OMAN, Sku.get(i).getText());
//		     Tc_7ToTestDimensionfeature(OMAN, Sku.get(i).getText());
//		     Tc_8ToTestViewfrontImage(OMAN, Sku.get(i).getText());
//		     Tc_8_1ToTestViewfrontImage(OMAN, Sku.get(i).getText());
		    //Tc_9ToTestImageRotation(OMAN, Sku.get(i).getText());}
		
	

	
	 /*
		@Test
		public void Tc_3_1ToTestViewLargeImage() throws InterruptedException{
		logger =extent.createTest("Test Case - To Verify that user is able to click on The View Larger Image Toggle for SKU: ");
		logger.log(Status.INFO, "Going to Click on View Large Image Toggle");
		omanPage.viewLargerImageFunc();
		logger.log(Status.PASS, "Test execution for View Large Image executed Successfully");
		}
		
		

	
		@Test
		public void Tc_3_2ToTestViewLargeImagewithTopView() throws InterruptedException{
		//logger =extent.createTest("Test Case - To Verify that user is able to click on The View Larger Image Toggle with View Top for SKU: "+sku+" And OMAN: "+OMAN);
			logger =extent.createTest("Test Case - To Verify that user is able to click on The View Larger Image Toggle with View Top Mode");
			Log4j.info("XXXXXXXXXXXXXXXXXXXXXXX                 TESTING VIEW LARGER FUNCTIONALITY WITH TOP VIEW                     XXXXXXXXXXXXXXXXXXXXXX");
			omanPage.getTopViewAttributeValue();
			omanPage.getTopViewAttributedataFrame();
			omanPage.clickOnTopToggle();
			logger.log(Status.PASS, "Image successfully clicked on View Top Mode");
			Log4j.info(
					"*****************************************************************************************************");
			logger.log(Status.INFO, "Now Going to click on - View large Image button, "
					+ "when Image is already opened in Top View");
			omanPage.viewLargerImageFunc();
		    logger.log(Status.INFO, "Going to Turn Off View Top Toggle");
		    omanPage.clickOnViewFrontToggle();
		    logger.log(Status.PASS, "View Top Mode successfully Turned Off");
		      }
		      
		
		@Test
		public void Tc_3_3ToTestViewLargeImagewithTopViewWithDimension() throws InterruptedException{
		//logger =extent.createTest("Test Case - To Verify that user is able to click on The View Larger Image Toggle with View Top for SKU: "+sku+" And OMAN: "+OMAN);
			logger =extent.createTest("Test Case - To Verify that user is able to click on The View Larger Image Toggle with View Top Mode");
			Log4j.info("XXXXXXXXXXXXXXXXXXXXXXX                 TESTING VIEW LARGER FUNCTIONALITY WITH TOP VIEW AND DIMESIONS ON                     XXXXXXXXXXXXXXXXXXXXXX");
			omanPage.getAttributesOfDimensionToggle();
			omanPage.clickDimensionToggle();
			Log4j.info("Getting the New Image Attributes");
			omanPage.getAttributesOfDimensionToggle();
			Log4j.info(
					"*****************************************************************************************************");
			omanPage.getTopViewAttributeValue();
			omanPage.getTopViewAttributedataFrame();
			omanPage.clickOnTopToggle();
			logger.log(Status.PASS, "Image successfully clicked on View Top Mode");
			Log4j.info(
					"*****************************************************************************************************");
			logger.log(Status.INFO, "Now Going to click on - View large Image button, "
					+ "when Image is already opened in Top View");
			omanPage.viewLargerImageFunc();
		    logger.log(Status.INFO, "Going to Turn Off View Top Toggle");
		    omanPage.clickOnViewFrontToggle();
		    logger.log(Status.PASS, "View Top Mode successfully Turned Off");
		      }
		
		*/
		@Test
		public void Tc_3_4ToTestViewLargeImageWithDimension() throws InterruptedException{
		//logger =extent.createTest("Test Case - To Verify that user is able to click on The View Larger Image Toggle with View Top for SKU: "+sku+" And OMAN: "+OMAN);
			logger =extent.createTest("Test Case - To Verify that user is able to click on The View Larger Image Toggle with View Top Mode");
			Log4j.info("XXXXXXXXXXXXXXXXXXXXXXX                 TESTING VIEW LARGER FUNCTIONALITY WITH DIMESIONS ON                     XXXXXXXXXXXXXXXXXXXXXX");
			omanPage.getAttributesOfDimensionToggle();
			omanPage.clickDimensionToggle();
			Log4j.info("Getting the New Image Attributes");
			omanPage.getAttributesOfDimensionToggle();
			Log4j.info(
					"*****************************************************************************************************");
			logger.log(Status.INFO, "Now Going to click on - View large Image button, "
					+ "when Image is already opened in Top View");
			omanPage.viewLargerImageFunc();
			omanPage.clickDimensionToggle();
			omanPage.getAttributesOfDimensionToggle();
		    logger.log(Status.PASS, "View Top Mode successfully Turned Off");
		 }
		
		
		/*

		//  @Test
		//  public void Tc_3_1ToTestViewLargeImageWithDimesion(String OMAN, String sku) throws InterruptedException{
		//  logger =extent.createTest("Test Case - To Verify that user is able to click on The View Larger Image Toggle with Dimension for SKU: "+sku+" And OMAN: "+OMAN);
		//  omaPage.skuDimensionImage();
		//  logger.log(Status.INFO, "Going to click on view Large Image Button");
		//  omaPage.viewLargerImage();
		//  logger.log(Status.PASS, "Successfully able to click on View larger Image with Dimension");
		//  }
		                     

		                      

		public void Tc_4ToTestZoomOutFeature(String OMAN, String SKu) throws InterruptedException{
		logger =extent.createTest("Test Case- To verify that user is able to click on Zoom In Toggle for SKU: "+SKu+" And OMAN: "+OMAN);
		logger.log(Status.INFO, "Going to Click on Zoom In Toggle");
		omaPage.skuZoomImage();
		logger.log(Status.PASS, "Image Successfully Zoomed In");
		}



		@Test
		public void Tc_4_1ToTestZoomInFeature() throws InterruptedException{
		//logger =extent.createTest("To Test The Zoom Out In Functionality for SKU: "+ Sku+" And OMAN: "+OMAN);
		Log4j.info(
					"*****************************************************************************************************");
		Log4j.info("XXXXXXXXXXXXXXXXXXXXXXX                 TESTING ZOOM IN FEATURE                      XXXXXXXXXXXXXXXXXXXXXX");
		logger.log(Status.INFO, "Going to validate the Toggle's Highlighted status and Its Resolution");
		omanPage.validationOfZoomToggle();
		logger.log(Status.PASS, "Image Successfully Zoomed IN");
		Log4j.info(
				"*****************************************************************************************************");
		}
		
		
		
			
		*/
	
	

		/*

		@Test
		public void Tc_5_1ToTestZoomInFeaturewithViewTop(String OMAN, String Sku) throws InterruptedException{
		logger =extent.createTest("To Test The Zoom In Image Functionality with To View Image for SKU: "+ Sku+" And OMAN: "+OMAN);
		logger.log(Status.INFO, "Going to Click on View Top Toggle");
		    omaPage.clickOnViewFront();
		        logger.log(Status.PASS, "Image successfully clicked on View Top Mode");
		        logger.log(Status.INFO, "Going to Click on Zoom In Toggle");
		omaPage.skuZoomImage();
		logger.log(Status.PASS, "Image Successfully Zoomed In");
		        logger.log(Status.INFO, "Going to Turn Off View Top Toggle");
		        omaPage.clickOnViewFront();
		        logger.log(Status.PASS, "View Top Mode successfully Turned Off");
		        logger.log(Status.INFO, "Going to Click on Zoom Out Toggle");
		omaPage.skuZoomImage();
		logger.log(Status.PASS, "Image Successfully Zoomed out");
		      

		}


		@Test
		public void Tc_6ToTestZoomInFeatureWithDimenion() throws InterruptedException{
		logger =extent.createTest("To Test The Zoom In Image with Dimension Functionality for SKU: "+sku+" And OMAN: "+OMAN);
		        logger.log(Status.INFO, "Going to Click on Zoom In Toggle");
		        omanPage.skuZoomImage();
		        logger.log(Status.PASS, "Image Successfully Zoomed In");
		        logger.log(Status.INFO, "Going to Click on Dimension Toggle");
		        omanPage.skuDimensionImage();
		        logger.log(Status.PASS, "Image Zoomed in With Dimension successfully");
		        omanPage.skuDimensionImage();
		        logger.log(Status.PASS, "Successfully able to click on Zoom In Image with Dimension off Functionality");


		}
		
		
		/*


		@Test

		    public void Tc_7ToTestDimensionfeature(String OMAN, String sku) throws InterruptedException{
		logger =extent.createTest("Test Case- To verify that the Dimensions of the Rendered Images for SKU: "+sku+" And OMAN: "+OMAN);
		logger.log(Status.INFO, "Going to Click on Dimesnion Toggle");
		omaPage.skuDimensionImage();
		logger.log(Status.PASS, "Successfully able to verify Dimension for rendered Image");

		   

		    }

		
		 
		@Test
		void Tc_8ToTestViewfrontImage(String OMAN, String sku) throws InterruptedException {

		      logger =extent.createTest("Test Case- To verify that the view Top Image Functionality for SKU: "+sku+" And OMAN: "+OMAN);
		      logger.log(Status.INFO, "Going to Click on View Top Toggle");
		      omaPage.clickOnViewFront();
		      logger.log(Status.PASS, "Image successfully clicked on View Top Mode");
		      logger.log(Status.INFO, "Going to Turn Off View Top Toggle");
		      omaPage.clickOnViewFront();
		      logger.log(Status.PASS, "View Top Mode successfully Turned Off");
		  
		 }

		@Test
		void Tc_8_1ToTestViewfrontImage(String OMAN, String sku) throws InterruptedException {
		      logger =extent.createTest("Test Case- To verify that the view Top Image Functionality with Dimension for SKU: "+sku+" And OMAN: "+OMAN);
		      try {
		logger.log(Status.INFO, "Going to Click on View Top Toggle");
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		      omaPage.clickOnViewFront();
		      logger.log(Status.PASS, "Image successfully clicked on View Top Mode");
		      logger.log(Status.INFO, "Going to Click on Dimension along with View Top Toggle");
		      omaPage.skuDimensionImage();
		      logger.log(Status.PASS, "Image successfully clicked on View Top with Dimension");
		      logger.log(Status.INFO, "Going to Turn Off View Top Toggle");
		      omaPage.clickOnViewFront();
		      logger.log(Status.PASS, "View Top Mode successfully Turned Off");
		  
		 }
		
		 
		@Test
		public void Tc_9ToTestImageRotation(){
		//logger =extent.createTest("Test Case- To verify that the Rotation of the Rendered Images for SKU: "+sku+" And OMAN: "+OMAN);
		logger.log(Status.INFO, "To Test the Rotation of the rendered Image");
		logger.log(Status.PASS, "The Rotation Type of the OMAN is : "+omanPage.getRotationTypeOfImage());
		logger.log(Status.INFO, "Fetching the Frame Sequence of the Image");
		logger.log(Status.INFO, "Here is the List of Framesequence "+ omanPage.getFrameSequenceOfImage());
		logger.log(Status.INFO, "To Test the Rotation feature: ");
		omanPage.validateImageRotation();
		logger.log(Status.PASS, "Successfully able to verify Rotation for rendered Image");

		}
		 
		 @Test
	public void Tc_6_1ToTestViewTop() throws InterruptedException {
		logger.log(Status.INFO, "Going to validate the Toggle's Highlighted status and Its Resolution for Zoomed out Image");
		Log4j.info("XXXXXXXXXXXXXXXXXXXXXXX                 TESTING TOP VIEW FUNCTIONALITY                     XXXXXXXXXXXXXXXXXXXXXX");
		omanPage.getTopViewAttributedataFrame();
		omanPage.getTopViewAttributeValue();
		omanPage.clickOnTopToggle();
		omanPage.getTopViewAttributedataFrame();	
		omanPage.getTopViewAttributeValue();
		omanPage.clickOnViewFrontToggle2();
		omanPage.getTopViewAttributedataFrame();	
		omanPage.getTopViewAttributeValue();
		
		}
		
		@Test
		public void Tc_4_2ToTestZoomInFeature() throws InterruptedException{
		//logger =extent.createTest("To Test The Zoom Out Image Functionality for SKU: "+ Sku+" And OMAN: "+OMAN);
		logger.log(Status.INFO, "Going to validate the Toggle's Highlighted status and Its Resolution for Zoomed out Image");
		Log4j.info("XXXXXXXXXXXXXXXXXXXXXXX                 TESTING ZOOM OUT FEATURE                      XXXXXXXXXXXXXXXXXXXXXX");
		omanPage.validationOfZoomOutToggle();
		logger.log(Status.PASS, "Image Successfully Zoomed IN");
		Log4j.info(
				"*****************************************************************************************************");
		}
		
		
		
		@Test
		public void Tc_5_1ToTestZoomInFeatureWithDimension() throws InterruptedException {
			logger.log(Status.INFO, "Going to validate the Toggle's Highlighted status and Its Resolution for Zoomed out Image");
			Log4j.info("XXXXXXXXXXXXXXXXXXXXXXX                 TESTING ZOOM IN FEATURE WITH DIMENSION                     XXXXXXXXXXXXXXXXXXXXXX");
			omanPage.getAttributesOfDimensionToggle();
			omanPage.clickDimensionToggle();
			Log4j.info("Getting the New Image Attributes");
			omanPage.getAttributesOfDimensionToggle();
			Log4j.info(
					"*****************************************************************************************************");
			Log4j.info("Now clicking on Zoom Toggle");
			omanPage.validationOfZoomToggle();
			Log4j.info("Getting the New Image Attributes");
			omanPage.getAttributesOfDimensionToggle();
			Log4j.info("XXXXXXXXXXXXXXXXXXXXXXX                 TESTING ZOOM OUT FEATURE DIMENSION                    XXXXXXXXXXXXXXXXXXXXXX");
			omanPage.validationOfZoomOutToggle();
			Log4j.info(
					"*****************************************************************************************************");
			omanPage.clickDimensionToggle();
			Log4j.info("Getting the New Image Attributes");
			omanPage.getAttributesOfDimensionToggle();
			
			Log4j.info(
					"*****************************************************************************************************");
			}
		
		*/
		

}
