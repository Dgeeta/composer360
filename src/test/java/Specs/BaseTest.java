package Specs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import Helper.Constants;
import Helper.ExcelUtils;
import Helper.PropertyFileUtility;
import Helper.BaseClass;
import Helper.CaptureScreenshot;

public class BaseTest {
	protected WebDriver driver;
	public ExtentTest logger;
	ExtentReports extent;
	ExtentHtmlReporter reporter;
	
	 public void setReports() {
		 reporter =new ExtentHtmlReporter("./test-output/ExtentReport.html");
		 reporter.config().setDocumentTitle("Automation Report");
		 reporter.config().setTheme(Theme.DARK);
		 extent = new ExtentReports();
		 extent.attachReporter(reporter);
		 extent.setSystemInfo("Host name", "Local Host");
		  extent.setSystemInfo("Environment", "QA Environment");
		  extent.setSystemInfo("Customer", "WSI PB 360");
		  extent.setSystemInfo("UserName",System.getProperty("User.name"));
		  extent.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
		  extent.setSystemInfo("User Location", System.getProperty("user.country"));
		 

		logger =extent.createTest("LoginTest");

		   
		

	 }
	
	 public void InitializeWebDriver(String browser, String url) throws Exception
	 { 
	      setReports();
	      
	  
	//Check if parameter passed from TestNG is 'firefox'
	if(browser.equalsIgnoreCase("firefox")){
	//create firefox instance
	System.setProperty("webdriver.gecko.driver", Constants.chromeDriver);
	driver = new FirefoxDriver();
	}

	//Check if parameter passed as 'chrome'
	else if(browser.equalsIgnoreCase("chrome")){
	//set path to chromedriver.exe
	System.setProperty("webdriver.chrome.driver",".\\src\\main\\resources\\ChromeDriver\\Latest\\chromedriver.exe");
	//create chrome instance
	driver = new ChromeDriver();
	}

	else{
	//If no browser passed throw exception
	throw new Exception("Either firefox/chrome is configured for this project currently or you have not installed one of these on your system.");
	}

	driver.get(url);
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	
	}

	  public WebDriver getDriver() {
	        return driver;
	    }
	
	    public void afterSuite() {
	        if(null != driver) {
	            driver.close();
	            driver.quit();
	        }
	    }
	

         @AfterClass
         public void tearDown() throws IOException {
     		
        	 extent.flush();
        	 
         }
         
         
         @AfterMethod
         public void evaluateStatus(ITestResult result) throws IOException {

         if (result.getStatus() == ITestResult.FAILURE) {

         logger.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
         logger.log(Status.FAIL, "TEST CASE ERROR IS " + result.getThrowable());
         String screenshotpath=CaptureScreenshot.getScreenshot(driver,result.getName());
         logger.fail("Test case has failed screenshot is below ", MediaEntityBuilder.createScreenCaptureFromPath(screenshotpath).build());

         System.out.println(screenshotpath);
         }
         else if (result.getStatus() == ITestResult.SUCCESS) {
         logger.log(Status.PASS, "Test Case PASSED IS " + result.getName());
         }
         }
         
	
	    
	    @DataProvider
	    public Object[][] getDataFromExcel() throws IOException {
	    	String[][] data = ExcelUtils.readFromExcel(PropertyFileUtility.getProperty("Oman_path"),"QT-249");
	    	return data; }
	    

	   
			   
	    
	    
	    
	    
	    
}
